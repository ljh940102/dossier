#df.org<-read.xlsx('./sampledata/kipris_sample5.xls',sheetIndex = 1, header = T, startRow = 8)

### calculate PFS for DOCDB family in kipris data###
#df[,1] is key value(application no. etc.)
#df[,2] is patent office (KR,JP,US,CN,EP...)
#df[,3] is DOCDB Family members delimted by "|"
pfs<-function(df){
  x<-df[,1]
  a<-paste(as.character(df[,2]),as.character(df[,3]),sep = "|")
  y<-sapply(strsplit(a,"|",fixed = TRUE), function(x) length(unique(substr(trimws(x),1,2))))
  rtn <-data.frame(x=as.factor(x),y=y)
  return(rtn)
}

##count |
#df[,1] is key value(application no. etc.)
#df[,2] is data delimted by "|"
cnt_pipe<-function(df){
  x<-df[,1]
  a<-paste(as.character(""),as.character(df[,2]),sep = "|")
  y<-sapply(strsplit(a,"|",fixed = TRUE), function(x) length(trimws(x)))
  rtn <-data.frame(x=as.factor(x),y=y-1)
  names(rtn)<-names(df)
  return(rtn)
}

##distinct count |
dcnt_pipe<-function(df){
  x<-df[,1]
  a<-paste("dummy",as.character(df[,2]),sep = "|")
  y<-sapply(strsplit(a,"|",fixed = TRUE), function(x) length(unique(gsub(".*,\\s*|\\).*", "", trimws(x)))))
  rtn <-data.frame(x=as.factor(x),y=y-1)
  names(rtn)<-names(df)
  return(rtn)
}

##distinct count IPC subclass delimited by |
#,로 split한 후 H01L과 같이 앞 4자리 substr 후 중복제거 카운트
dcnt_IPC<-function(df){
  x<-df[,1]
  a<-paste("dummy",as.character(df[,2]),sep = ",")
  y<-sapply(strsplit(a,",",fixed = TRUE), function(x) length(unique(substr(trimws(x),1,4))))
  rtn <-data.frame(x=as.factor(x),y=y-1)
  names(rtn)<-names(df)
  return(rtn)
}

##scoring from legal status
#df[,1] is key value(application no. etc.)
#df[,2] is legal status
sc_lg <- function(df){
  df[,2]<-as.character(df[,2])
  df[,2][df[,2]=="등록"] <- 7
  df[,2][df[,2]=="소멸 (존속기간만료)"] <- 6
  df[,2][df[,2]=="소멸 (등록료불납)"] <- 5
  df[,2][df[,2]=="공개"] <- 4
  df[,2][df[,2]=="포기"] <- 3
  df[,2][df[,2]=="취하"] <- 2
  df[,2][df[,2]=="거절"] <- 1
  return(df)
}

std_dt<-function(x){
  rtn <- (x-mean(x))/(sd(x)/sqrt(length(x)))
  return(rtn)
}

##factor analysis using kipris data

##분석목적: 데이터 셋에서 정성분석을 위해 데이터를 선정하려는 경우,
## 여러 특허 항목의 정량지표를 분석하여 단일 혹은 2-3개의 factor로 인자 분석하여 데이터 선정에 용이하도록 하고,
## 인자 분석의 데이터로 산점도 나타내어 시각적으로 군집화 가능한 back data 제공을 목적으로 함

##분석세부:
#Kipris download 데이터는 한국특허와 해외특허로 다운 가능
#한국특허로 다운받을 때의 컬럼 구성과 해외특허로 다운받을 때 컬럼 구성이 다르므로 2가지로 나눠 분석한다.

#한국특허 인자분석(factor analysis)를 위해 
#발명자수,발명자국적수,출원인수,출원인국적수,IPC서브클래스수,법적상태점수 정량데이터 추출
#상기 각각은 발명협력도, 발명대외협력도, 출원협력도,출원대외협력도,융합기술척도,권리점수로 의미부여 가능

#해외특허 인자분석(factor analysis)를 위해 
#발명자수,발명자국적수,출원인수,출원인국적수,IPC서브클래스수,PFS(patent family size) 정량데이터 추출
#상기 각각은 발명협력도, 발명대외협력도, 출원협력도,출원대외협력도,융합기술척도,시장성(해외)로 의미부여 가능

#./sampledata/kipris_sample5.xls파일은 한국특허 다운로드 데이터이다.

#df.org<-read.xlsx('./sampledata/kipris_sample5.xls',sheetIndex = 1, header = T, startRow = 8)
##names(df.org)
# [1] "순서"          "출원번호"      "출원일자"      "IPC분류"      
#[5] "공개번호"      "공개일자"      "공고번호"      "공고일자"     
#[9] "등록번호"      "등록일자"      "심사진행상태"  "지정국"       
#[13] "법적상태"      "출원인"        "발명자.고안자" "대리인"       
#[17] "우선권정보"    "발명의명칭"    "초록" 

#다변량 데이터의 생성
mvadt<-function(df.org,lang){
  
  if(lang =="en"){
    df.it<-data.frame(appln=df.org$출원번호,it=df.org$발명자.고안자)
    df.at<-data.frame(appln=df.org$출원번호,at=df.org$출원인)
    df.ip<-data.frame(appln=df.org$출원번호,ip=df.org$IPC분류)
    df.lg<-data.frame(appln=df.org$출원번호,lg=df.org$DOCDB.패밀리정보)
  }else{
    df.it<-data.frame(appln=df.org$출원번호,it=df.org$발명자.고안자)
    df.at<-data.frame(appln=df.org$출원번호,at=df.org$출원인)
    df.ip<-data.frame(appln=df.org$출원번호,ip=df.org$IPC분류)
    df.lg<-data.frame(appln=df.org$출원번호,lg=df.org$법적상태)
  }
  df.it.cnt<-cnt_pipe(df.it)
  df.itc.cnt<-dcnt_pipe(df.it)
  df.at.cnt<-cnt_pipe(df.at)
  df.atc.cnt<-dcnt_pipe(df.at)
  df.ip.cnt<-dcnt_IPC(df.ip)
  df.lg.cnt<-sc_lg(df.lg)
  
  a<-merge(df.it.cnt,df.itc.cnt,by = 'appln')
  a<-merge(a,df.at.cnt,by = 'appln')
  a<-merge(a,df.atc.cnt,by = 'appln')
  a<-merge(a,df.ip.cnt,by = 'appln')
  a<-merge(a,df.lg.cnt,by = 'appln')
  a$lg<-as.numeric(a$lg)
  rownames(a)<-a$appln
  rtn <- a[,-1]
  names(rtn)<-c("발명협력도","발명대외협력도","출원협력도","출원대외협력도","기술융합도","권리점수")
  return(rtn)
  
}

#standardized data->MDS, 2 factor analysis->hcluster,k-means
# mf = c("pca","fap","fal"), hc = c("km","hc","kd")

library(stats) # for PCA, fal(factor analysis using maximum likelihood method)
library(psych) # for fap(factor analysis using principal factor method)
library(GPArotation) # for fap(factor analysis using principal factor method)

library(cluster) #for k-medoids (function pam)
library(smacof)

#library(car)

#df is num and has appln rownames
#ex) df
#                it.x it.y at.x at.y ip lg
#10-1985-0008672    2    1    1    1  1  5
#10-1985-0008676    2    1    1    1  1  5
#10-1985-0008798    2    1    1    1  1  5
cluster_fd<-function(df,mf="pca",hc="km",n=2){
  zdf <- scale(df, center = T, scale = T)
  
  if(hc =="km"){
    km <- kmeans(zdf,n)
    cl = km$cluster
    tl = "k-means clustering"
  }else if(hc =="hc"){
    hc <- hclust(dist(zdf),method = "single")
    cl<-cutree(hc,n)
    tl<-"hierarchical clustering"
  }else if(hc == "kd"){
    kd <- pam(zdf,n)
    cl = kd$clustering
    tl = "k-medoids clustering"
  }
  
  if(mf=="mds"){
    zdf.dist<-dist(zdf,method = "euclidean")
    mds<- cmdscale(zdf.dist,k = 2)
    x = mds[,1]
    y = mds[,2]
    
    tl <- paste(tl,"in MDS analysis coordinate",sep = " ")
    xl <-"dim1"
    yl <-"dim2"
  }else if(mf =="pca"){
    pca=princomp(zdf,cor = T,scores = T)
    x = pca$scores[,1]
    y = pca$scores[,2]
    
    tl <- paste(tl,"in 2-principal components analysis coordinate",sep = " ")
    xl <-"pc1"
    yl <-"pc2"
  }else if(mf =="fap"){
    fap <-principal(zdf,nfactors = 2)
    x = fap$scores[,1]
    y = fap$scores[,2]
    
    tl <- paste(tl,"in 2-factors analysis(PCM) coordinate",sep = " ")
    xl <-"ftr1"
    yl <-"ftr2"
  }else if(mf =="fal"){
    fal=factanal(zdf,factors = 2, rotation = "none", scores = "Bartlett")
    #default: *
    #rotation = c(*"varimax","promax","none")
    #scores = c(*"none","regression","Bartlett")
    x = fal$scores[,1]
    y = fal$scores[,2]
    
    tl <- paste(tl,"in 2-factors analysis(MLM) coordinate",sep = " ")
    xl <-"ftr1"
    yl <-"ftr2"
  }
  
  
  cl.df<-as.data.frame(cl)
  x.df<-as.data.frame(x)
  y.df<-as.data.frame(y)
  
  xdf<-data.frame(appn=rownames(x.df),x=x.df)
  ydf<-data.frame(appn=rownames(y.df),y=y.df)
  cdf<-data.frame(appn=rownames(cl.df),cluster=cl.df)
  rtn<-merge(xdf,ydf,by="appn")
  rtn<-merge(rtn,cdf,by="appn")
  rtn <- as.data.frame(rtn)
  
  
  plot(x,y, col = cl+1, pch = cl, main = tl, xlab = xl, ylab = yl)
  abline(h=0, v=0, lty = 3)
  
  return(rtn)
  
}

