library(shiny)

shinyUI(fluidPage(
  includeHTML("../../main_index/header.html"),
  # 사이드바 UI
  sidebarLayout(
    sidebarPanel(
      width=3,
      radioButtons("lang", "원하시는 문서의 타입을 선택해 주세요",
                   c("한글" = "kor",
                     "영문" = "en"
                   ), inline=T),
      
      tags$a(href ="/main/sampledata/kipris_sample5.xls", "샘플"),
      
      fileInput("file", label = h3("File input")),
      numericInput("firstrow", label ="입력하시는 데이터가 시작되는 행의 수를 입력해주세요",
                   value = 8),
      
      conditionalPanel(
        condition = "input.tab=='multi-variable'",
        selectInput("clus_way", label ='원하시는 클러스터링 기법을 선택해 주세요',
                    choices = list("k-means" = "km", "hclust" = "hc", "pam" = "kd"),
                    selected = "km"),
        
        selectInput("test_way", label ='원하시는 축소 기법을 선택해 주세요',
                    choices = list("pca" = "pca", "fap" = "fap", "fal" = "fal"),
                    selected = "pca"),
        
        sliderInput("num_k_means", label = h3("원하는 군집의 수를 입력해주세요"),
                    min = 1,max = 5, value = 3),
        actionButton("btn_1", "Go!")
      ),
      
      conditionalPanel(
        condition = "input.tab=='map'",
        htmlOutput("text"),
        htmlOutput("compute_year"),
        actionButton("btn_2", "Go!")
      )
      
    ),
    
    mainPanel(
      tabsetPanel(id="tab",type = "tabs", 
                  tabPanel("multi-variable", plotOutput('multi_var'),DT::dataTableOutput("multi_var_table")),
                  tabPanel("map", plotOutput('map')),
                  tabPanel("help",includeHTML("../../main_index/analysis4_help.html"))
      )
    )
  )
)
)