options(shiny.maxRequestSize=100*1024^2) # it makes shiny use big memory
setwd('../../')
source("MVA.R")
library(DT)

######### read local database that is about IPC information

shinyServer(function(input, output) {
  
  import_data <- reactive({
    
    df<-get_kipris(input$file$datapath)
  })
  
  rtn.l <-reactive({
  
    df <- import_data()
    temp_data <- mvadt(df,input$lang)
    result<-cluster_fd(temp_data,mf=isolate(input$test_way),hc=isolate(input$clus_way),n=isolate(input$num_k_means))
    result
  })
  
  preprocess_map <-reactive({
    
    df<- import_data()
    temp <- dat(df)
    temp
  })
  
  output$multi_var <- renderPlot({
    
    input$btn_1
    rtn<-rtn.l()
    rtn
  })
  
  output$multi_var_table<- DT::renderDataTable({
    
    input$btn_1
    rtn<-rtn.l()
  })
  
  output$map <- renderPlot({
    
    input$btn_2
    temp <- preprocess_map()
    gpt(temp,as.integer(input$year))
  })
  
  output$compute_year <- renderUI({
    
    input$btn_2
    temp <- preprocess_map()
    year <- unique(names(temp))
    numericInput("year","", value = as.integer(min(year)))
  })
  
  output$text <- renderUI({
    temp <- preprocess_map()
    year <- unique(names(temp))
    temp_year <- do.call(paste,as.list(year))
    HTML(paste("분석을 원하는 연도를 입력해주세요","\n", "분석 가능한 연도 :","\n",temp_year, sep=""))
  })

}
)