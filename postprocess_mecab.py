#!/usr/bin/python
# -*- coding:utf-8 -*-

from konlpy.tag import Twitter, Mecab
import itertools


tagger = Mecab()

#uncompound = set([unicode(l.strip(),'utf-8') for l in open("./uncompound.txt") if not l.strip().startswith('#')])


def test(sent):
    return tagger.pos(sent)


def nouns(sent):
    words = [filtering(words) for words in tagger.pos(sent, flatten=False)]
    return [m for m,p in itertools.chain(*words) if p.startswith('N') or p=='SL']
    
    
def merge_nouns(a, b):
    '''
    avoiding too much split
    '''
    p_a = (len(a[0])==1 and a[1].startswith("N")) or a[1]=="NNP"
    p_b = (len(b[0])==1 and b[1].startswith("N")) or b[1]=="NNP"

    if p_a and p_b:
        return [(a[0]+b[0], "NNP")]
        
    #if a[0]+','+b[0] in uncompound:
    #    return [(a[0]+b[0], "NNG")]

    return [a,b]
    
    
def filtering(words):
    new_words = []
    prev = words[0]
    for i in range(1, len(words)):
        res = merge_nouns(prev, words[i])
        if len(res) > 1:
            new_words.append(res[0])
        prev = res[-1]
    new_words.append(prev)
    return new_words
