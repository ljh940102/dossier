# test jwlee
#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

##################################################
# LOADING LIBRARIES AND FUNCTIONS
##################################################
library(DBI)
library(RPostgreSQL)
library(ggplot2)
library(shiny)

# Define UI for application that draws a plot
ui <- fluidPage(
  
  # Application title
  titlePanel("Trend Of db_patent"),
  
  # Sidebar with a slider input for number of bins 
  sidebarLayout(
    sidebarPanel(
      sliderInput("bins",
                  "Number of last years:",
                  min = 2,
                  max = 20,
                  value = 2)
    ),
    
    # Show a plot of the generated distribution
    mainPanel(
      plotOutput("distPlot")
    )
  )
)

# Define server logic required to draw a histogram
server <- function(input, output) {
  con <- dbConnect("PostgreSQL", host="xxx.242.170.xxx",
                   port="5432",dbname="db_patent",
                   user="usr",password="pw")
  qry <- "select * from work_2017.w_trend_appln;"
  srcData <- dbGetQuery(con,qry)
  dbDisconnect(con)    
  output$distPlot <- renderPlot({
    # generate bins based on input$bins from ui.R
    yr.new<-c((as.numeric(max(srcData$yr))-20):as.numeric(max(srcData$yr)))
    srcData1 <- srcData[srcData$yr==(yr.new[21]),]
    if (input$bins >1){
      for (i in 2:input$bins ){
        srcData1 <- rbind(srcData1, srcData[srcData$yr==(yr.new[22-i]),])
      }
    }
    
    ggplot(data = srcData1, aes(x = factor(yr), y = cnt, color = nat_cd))+geom_line(aes(group=nat_cd))+geom_point()+labs(x="appln_year",y="appln_count",color = "office")
  })
}

# Run the application 
shinyApp(ui = ui, server = server)

