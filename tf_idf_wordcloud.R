# 
library(wordcloud2)
library(tm)
tfidf_wordcloud <- function(nouns,input_size){
  
  names(nouns)<-1:length(nouns)
  nouns.t<-table(unlist(nouns))
  docs.corp <- Corpus(VectorSource(nouns))
  TDM_TfIdf <- TermDocumentMatrix(docs.corp, control=list(removePuctuation = TRUE, removeNumbers = TRUE, stopwords = TRUE, weighting = weightTfIdf))
  a<-as.matrix(TDM_TfIdf)
  a.1<-apply(a,1,max)
  a.word<-names(which(a.1>0.4)) # set tf-idf value
  a.t<-nouns.t[a.word]

  #exampel 1 (except more than 50 word)
  
  p<-wordcloud2(a.t[which(a.t<50)],color="random-light",size=input_size)
  
  # example 2 ( except topmost 10percent word)
  #a.word.2<-names(tail(sort(a.t,decreasing = T),floor(length(a.t)*0.9)))
  #a.t.2<-a.t[a.word.2]
  # p <- wordcloud2(a.t.2,color="random-light",size=input_size)
  return(p)
  
}
